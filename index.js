const env = require('./.env');
const Telegraf = require('telegraf')
const axios = require("axios");


const bot = new Telegraf(env.token)
bot.start((ctx) => {
  const from = ctx.update.message.from
  console.log(from);
  ctx.reply(`Welcome, ${from.first_name}!`);
  ctx.reply(`What kind of Gif do you want?`);
})

bot.on('text', async (ctx, next) => {
    const texto = ctx.update.message.text;
    try {
        const res = await axios.get(`${env.giphyAPI}/v1/gifs/random?api_key=${env.giphyToken}&tag=${texto}&rating=g&fmt=json`);
        ctx.reply("Searching...");
        ctx.replyWithVideo({url: `${res.data.data.image_mp4_url}`})
      } catch (error) {
        ctx.reply("Fail!");    
      }
    
})

bot.launch()